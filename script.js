(function(angular) {
    'use strict';
    angular.module('testApp', []);

    function FormController($http) {
        var ctrl = this;

        ctrl.radioChange = function (s) {
            ctrl.flag = s;
        };

        ctrl.newItem = function(name, title, number, flag) {

            var data = {
                name: name,
                title: title,
                number: number,
                flag: flag
            };

            var config = {
                headers : {
                    'Content-Type': 'application/json',
                    'X-Appery-Database-Id': '58226eeee4b0a696f3532f3d'
                }
            };
            $http.post('https://api.appery.io/rest/1/db/collections/testCollection', data, config)
                .success(function (data, config) {
                    console.log('ok');
                })
                .error(function (data, config) {
                    console.log('error');
                });
        };
    }


    angular.module('testApp').component('formDetail', {
        template: '<div><button ng-click="click = !click">Add row</button><form ng-show="click"><div><label>Name</label><input ng-model="name" type="text"></div><div><label>Title</label><input ng-model="title" type="text"></div><div><label>Number</label><input ng-model="number" type="text"></div><div><label>Flag</label><input ng-model="flag" ng-change="radioChange(\'true\')" type="radio" id="flagTrue" name="flag" value="true"><label for="flagTrue">true</label><input ng-model="flag" ng-change="radioChange(\'false\')" type="radio" id="flagFalse" name="flag" value="false"><label for="flagFalse">false</label></div><button ng-click="$ctrl.newItem(name, title, number, flag)">Save</button></form></div>',
        controller: FormController,
    });

    function TestListController($http, $scope, $element, $attrs) {
        var ctrl = this;
        ctrl.date = [];
        ctrl.list = [];

        $http({method: 'GET', url: 'https://api.appery.io/rest/1/db/collections/testCollection', headers: {'X-Appery-Database-Id':'58226eeee4b0a696f3532f3d'}, dataType: 'json'})
            .success(function (result) {
                ctrl.list = result;
                for (var i = 0; i < ctrl.list.length; i++) {
                    ctrl.date[i] = new Date(ctrl.list[i]._createdAt);
                    ctrl.list[i]._createdAt = ctrl.date[i];
                }
                console.log(ctrl.list);
            })
            .error(function (result) {
                console.log('error');
            });

        ctrl.delete = function(test) {
            var idx = ctrl.list.indexOf(test);
            if (idx >= 0) {
                ctrl.list.splice(idx, 1);
            }
        };
    }

    angular.module('testApp').component('testList', {
        template: '<div><table><thead><tr><th>Name</th><th>Title</th><th>Number</th><th>Flag</th><th>Date</th><th></th></tr></thead><tbody><tr ng-repeat="test in $ctrl.list" test="test"><td>{{test.name}}</td><td>{{test.title}}</td><td>{{test.number}}</td><td>{{test.flag}}</td><td>{{test._createdAt | date : \'dd MM yyyy\'}}</td><td><button ng-click="$ctrl.delete(test)">Delete</button></td></tr></tbody></table></div>',
        controller: TestListController
    });
})(window.angular);

